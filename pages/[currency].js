import Layout from "../components/Layout";
import Image from "next/image";

export default function Currency({ res }) {

  const myLoader = ({ src }) => {
    return res.logo_url;
  }

  return (
    <Layout page={"Page " + res.name}>
      <div className="relative hover:shadow-md p-8 border border-blue-300 sm:rounded-3xl bg-blue-100 md:w-auto flex-1 mx-5">
        <div className="text-center">
          <Image
            loader={myLoader}
            src={res.logo_url}
            alt={res.name}
            className="w-20 h-20 mx-auto mb-6"
            width="150"
            height="150"
          />
        </div>
        <h2 className="text-2xl mb-6 uppercase tracking-wider">{res.name}</h2>
        <p>{res.description}</p>
        <p className="pt-5 text-blue-500">
          <a target="_blank" rel="noreferrer" href={res.reddit_url}>
            {res.reddit_url}
          </a>
        </p>
      </div>
    </Layout>
  );
}

export async function getServerSideProps({ query }) {
  try {
    const res = await fetch(`https://api.nomics.com/v1/currencies?key=0728ce4da372667be035a6e4efb4a1404096e4f6&ids=${query.currency}&attributes=id,name,logo_url,description,reddit_url`);
    const result = await res.json();

    return {
      props: { res: result[0] },
    };
  } catch (err) {
    console.error(err);
  }
}
