import Layout from '../components/Layout';

export default function About() {
  return (
    <Layout page="A propos">
      <h1 className="text-4xl">About</h1>
      <br />
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin volutpat ut lacus eu pulvinar. Suspendisse volutpat fringilla tortor, non tincidunt lorem fermentum ac. Duis at tempus orci. In hac habitasse platea dictumst. In at aliquet turpis, eu gravida magna. Aenean tempus tellus augue. Duis id congue turpis. Vivamus rutrum risus vel mollis rhoncus. Suspendisse eleifend dictum dui, eu euismod ligula pretium eget. Praesent turpis turpis, rutrum non aliquet at, aliquam ac purus. Duis rutrum sem leo, eu cursus nunc feugiat vel. Quisque aliquet vehicula orci in dignissim.
      </p>
      <br />
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin volutpat ut lacus eu pulvinar. Suspendisse volutpat fringilla tortor, non tincidunt lorem fermentum ac. Duis at tempus orci. In hac habitasse platea dictumst. In at aliquet turpis, eu gravida magna. Aenean tempus tellus augue. Duis id congue turpis. Vivamus rutrum risus vel mollis rhoncus. Suspendisse eleifend dictum dui, eu euismod ligula pretium eget. Praesent turpis turpis, rutrum non aliquet at, aliquam ac purus. Duis rutrum sem leo, eu cursus nunc feugiat vel. Quisque aliquet vehicula orci in dignissim.
      </p>
    </Layout>
  )
}